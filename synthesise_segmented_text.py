#!/usr/bin/env python3
#
# Copyright (c) 2022 − Present Guillaume Bernard <contact@guillaume-bernard.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
import argparse
import os
from fractions import Fraction
from pathlib import Path
from typing import Iterator, List

import pandas
from matplotlib import pyplot
from pandas import DataFrame
from tqdm import tqdm


def over_segment_ocred_documents(
    ocr_files_directory: str, split_every_x_percent_lines: int, output_directory: str
):
    files_in_directory = get_files_in_directory(ocr_files_directory)
    number_of_chunks = (
        Fraction(split_every_x_percent_lines / 100).limit_denominator(10).denominator
    )
    for id_file, _ in tqdm(
        files_in_directory.items(),
        desc=f"Reproduce over-segmentation on ’{'/'.join(list(Path(ocr_files_directory).parts)[-2::])}’",
    ):
        with open(
            Path(ocr_files_directory) / files_in_directory[str(id_file)],
            encoding="utf-8",
        ) as ifile:
            file_lines = remove_lines_without_text(ifile.readlines())
            number_of_lines_per_chunk = len(file_lines) // number_of_chunks
            chunks = file_lines
            if number_of_lines_per_chunk > 0:
                chunks = divide_list_in_n_chunks(
                    file_lines, number_of_lines_per_chunk + 1
                )  # to avoid one line chunks
            write_chunks_to_disk(
                id_file, chunks, split_every_x_percent_lines, output_directory
            )


def divide_list_in_n_chunks(the_list, number_of_lines_per_chunk):
    for i in range(0, len(the_list), number_of_lines_per_chunk):
        yield the_list[i : i + number_of_lines_per_chunk]


def remove_lines_without_text(lines_of_text: List[str]):
    return [text for text in lines_of_text if text_is_valid(text)]


def text_is_valid(text: str):
    return text not in ("\n", "\f")


def write_chunks_to_disk(
    document_id: int,
    chunks: Iterator[List[str]],
    split_every_x_percent_lines: int,
    output_directory: str,
):
    Path(output_directory).mkdir(parents=True, exist_ok=True)
    for chunk_id, chunk in enumerate(chunks, start=1):
        with open(
            Path(output_directory)
            / f"{document_id}.over_segmented_{split_every_x_percent_lines}_percent_chunk_{chunk_id}.txt",
            mode="w",
            encoding="utf-8",
        ) as output_file:
            output_file.writelines(chunk)


def export_lines_in_documents_statistics(
    ocr_files_directory: str, output_statistics_filename: str
):
    lines_in_files_df = count_lines_of_each_file(ocr_files_directory)
    bins_of_sizes_df = split_lines_into_bins(lines_in_files_df, 10)
    figure = plot_bins_of_lines_in_files(
        bins_of_sizes_df, Path(ocr_files_directory).stem
    )
    if output_statistics_filename is not None:
        Path(output_statistics_filename).parent.mkdir(exist_ok=True, parents=True)
        figure.savefig(f"{output_statistics_filename}.png")
        bins_of_sizes_df.to_csv(f"{output_statistics_filename}.csv")


def count_lines_of_each_file(ocr_files_directory) -> DataFrame:
    files_in_directory = get_files_in_directory(ocr_files_directory)
    counts = {}
    for id_file, _ in files_in_directory.items():
        with open(
            Path(ocr_files_directory) / files_in_directory[str(id_file)],
            encoding="utf-8",
        ) as ifile:
            counts[id_file] = lines_in_file(ifile)
    return DataFrame.from_dict(
        counts, orient="index", columns=["nb_lines"]
    ).sort_values(by="nb_lines")


def get_files_in_directory(ocr_files_directory):
    return {file.split(".")[0]: file for file in os.listdir(ocr_files_directory)}


def lines_in_file(input_file):
    count = 0
    for line in input_file:  # start at 0 as there is a final \f in tesseract output
        if line != "\n":
            count += 1
    return count


def split_lines_into_bins(lines_in_files: DataFrame, number_of_bins: int) -> DataFrame:
    df = pandas.cut(lines_in_files["nb_lines"], bins=number_of_bins)
    df = df.groupby(df).count().to_frame()
    df.columns = ["nb_documents"]
    df["ratio"] = round(df["nb_documents"] / sum(df["nb_documents"]) * 100, 3)
    return df


def plot_bins_of_lines_in_files(bins_of_sizes: DataFrame, language: str):
    bins_of_sizes.index = [
        f"{str(int(float(interval[0])))} → {str(int(float(interval[1])))}"
        for interval in [
            str(i).replace("(", "").replace("]", "").replace(" ", "").split(",")
            for i in bins_of_sizes.index
        ]
    ]

    figure, ax1 = pyplot.subplots()

    color = "tab:red"
    ax1.set_title(
        f"Length of documents in ’{language}’, distributed in {len(bins_of_sizes)} bins."
    )
    ax1.set_xlabel("Length of document interval (in number of lines)", color=color)
    ax1.set_ylabel("Number of documents in the bin", color=color)
    ax1.tick_params(axis="y", labelcolor=color)
    ax1.tick_params(axis="x", labelrotation=45)
    ax1.bar(
        x=bins_of_sizes.index,
        height=bins_of_sizes["nb_documents"],
        color=color,
        alpha=0.5,
    )

    ax2 = ax1.twinx()

    color = "tab:blue"
    ax2.set_ylabel("% of the whole dataset", color=color)
    ax2.tick_params(axis="y", labelcolor=color)
    ax2.set_ylim(0, 100)
    ax2.scatter(bins_of_sizes.index, y=bins_of_sizes["ratio"], color=color)

    figure.tight_layout()
    pyplot.plot()
    return figure


def main(args: argparse.Namespace):

    if args.output_statistics_filename:
        export_lines_in_documents_statistics(
            args.ocr_files_directory, args.output_statistics_filename
        )

    over_segment_ocred_documents(
        args.ocr_files_directory,
        args.split_every_x_percent_lines,
        args.segmented_ocr_files_directory,
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Split OCRed document into multiple ones to simulate over-segmentation in documents."
    )
    parser.add_argument(
        "--ocr-files-directory",
        required=True,
        type=str,
        help="Name of the directory where OCRed files are located in.",
    )
    parser.add_argument(
        "--segmented-ocr-files-directory",
        required=True,
        type=str,
        help="Where to put the artificially segmented files.",
    )
    parser.add_argument(
        "--split_every_x_percent_lines",
        required=True,
        type=int,
        help="Split the input document every X percent lines (integer between 0 and 100).",
        choices=range(0, 101),
        metavar="[0-100]",
    )
    parser.add_argument(
        "--output-statistics-filename",
        required=False,
        type=str,
        help="Where to save the number of lines statistics files (without extension).",
    )

    main(parser.parse_args())
