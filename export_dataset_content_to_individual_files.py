#!/usr/bin/env python3
#
# Copyright (c) 2022 − Present Guillaume Bernard <contact@guillaume-bernard.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import argparse
import html
import re
from pathlib import Path

from document_tracking_resources.corpus import (
    NewsCorpusWithDenseFeatures,
    TwitterCorpusWithDenseFeatures,
)


def main(args: argparse.Namespace):
    corpus_classes = {
        "news": NewsCorpusWithDenseFeatures,
        "twitter": TwitterCorpusWithDenseFeatures,
    }

    corpus_class = corpus_classes.get(args.dataset_type)
    corpus = corpus_class.from_pickle_file(args.corpus)

    output_directory = Path(args.output_directory)
    output_directory.mkdir(parents=True, exist_ok=True)
    for document_id, document_row in corpus.documents.iterrows():
        with open(
            output_directory / f"{document_id}.txt", mode="w", encoding="utf-8"
        ) as output_file:
            text = document_row[args.field]
            output_file.write(re.sub(" +", " ", html.unescape(text)))


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="""
        Extract text from a corpus of documents into files. Each file will contain the content of the field exported,
        the exported file will be named after the identifier of the exported document.
    """
    )

    parser.add_argument(
        "--corpus", type=str, required=True, help="Path to the pickle dataset file."
    )
    parser.add_argument(
        "--output-directory", type=str, required=True, help="Where to export documents."
    )
    parser.add_argument(
        "--dataset-type",
        choices=["twitter", "news"],
        required=True,
        type=str,
        help="The kind of dataset to process. "
        "‘twitter’ will use the ’TwitterCorpus’ class, the ‘Corpus’ class otherwise.",
    )
    parser.add_argument(
        "--field",
        "-f",
        type=str,
        required=True,
        choices=["text", "title"],
        help="The field of the text to export. It’s a text field name from the dataset.",
    )

    main(parser.parse_args())
