#!/bin/bash

# Run Tesseract OCR on all png images in the target directory, with the given language.
# Param 1: target directory, the one containing .png images
# Param 2: language in which are written the texts in the target directory images

for file in "${1}"/*.png; do
  tesseract -l "${2}" "${file}" "${file%%.png}"
done
