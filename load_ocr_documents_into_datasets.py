#!/usr/bin/env python3
#
# Copyright (c) 2022 − Present Guillaume Bernard <contact@guillaume-bernard.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import argparse
import logging
import os
import re
from pathlib import Path

from document_tracking_resources.corpus import (
    NewsCorpusWithDenseFeatures,
    TwitterCorpusWithDenseFeatures,
)
from tqdm import tqdm

logger = logging.getLogger(__name__)
formatter = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(funcName)s in %(module)s − %(message)s"
)

console_handler = logging.StreamHandler()
console_handler.setFormatter(formatter)
logger.addHandler(console_handler)
logger.setLevel(logging.DEBUG)


def main(args: argparse.Namespace):

    corpus_classes = {
        "news": NewsCorpusWithDenseFeatures,
        "twitter": TwitterCorpusWithDenseFeatures,
    }
    corpus_class = corpus_classes.get(args.dataset_type)
    corpus = corpus_class.from_pickle_file(args.corpus)

    files_in_directory = {
        file.split(".")[0]: file for file in os.listdir(args.ocr_files_directory)
    }
    for index, document in tqdm(
        corpus.documents.iterrows(),
        total=len(corpus),
        desc=f"Load OCR documents ({Path(args.corpus).stem})",
    ):
        try:
            with open(
                Path(args.ocr_files_directory) / files_in_directory[str(index)],
                encoding="utf-8",
            ) as ifile:
                corpus.documents.at[index, args.field] = re.sub(
                    " +", " ", ifile.read().replace("\n", " ").replace("\f", " ")
                )
        except KeyError:
            logger.warning(
                "Error: not able to set the OCRed version of the text of ’{index}’. "
                "Original text: ‘%s’",
                document[args.field],
            )

    Path(args.output_file).parent.mkdir(parents=True, exist_ok=True)
    corpus.as_dataframe().to_pickle(args.output_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="""
        Load text files to populate the specified field of the input corpus.
    """
    )

    parser.add_argument(
        "--corpus", type=str, required=True, help="Path to the pickle dataset file."
    )
    parser.add_argument(
        "--ocr-files-directory",
        type=str,
        required=True,
        help="Where text files are located, with document id as name (before any extension).",
    )
    parser.add_argument(
        "--dataset-type",
        choices=["twitter", "news"],
        required=True,
        type=str,
        help="The kind of dataset to process. "
        "‘twitter’ will use the ’TwitterCorpus’ class, the ‘Corpus’ class otherwise",
    )
    parser.add_argument(
        "--field",
        "-f",
        type=str,
        required=True,
        choices=["text", "title"],
        help="The field of the text to import. It’s a text field name from the dataset.",
    )
    parser.add_argument(
        "--output-file",
        "-o",
        type=str,
        required=True,
        help="The name of the new dataset with the updated field.",
    )

    main(parser.parse_args())
