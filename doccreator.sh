#!/usr/bin/env bash

# The DocCreator tool is a tool to damage images to look like historical documents.
# It is mentioned in the following study:
# Linhares Pontes, Elvys, Ahmed Hamdi, Nicolas Sidere, et Antoine Doucet. 2019.
# « Impact of OCR Quality on Named Entity Linking ». In Digital Libraries
# at the Crossroads of Digital Information for the Future, édité par Adam Jatowt,
# Akira Maeda, et Sue Yeon Syn, 11853:102‑15. Lecture Notes in Computer Science.
# Cham: Springer International Publishing. https://doi.org/10.1007/978-3-030-34058-2_11.

# We use it to damage pre-produced images.
# The use with container is documented here: https://github.com/DocCreator/DocCreator/tree/master/bundlers
# In case it is not available, https://archive.softwareheritage.org/swh:1:dir:796ef98f05b4d6b26b2cbb75f52dacd4ec6bd2ee
# A pre-built container image is available at guilieb/doccreator:latest. In case it is not available, you will
# have to build it on your own by following the instruction given in the above links.

source "$(dirname "${BASH_SOURCE[0]}")/.env"

podman run \
  --rm \
  -d \
  -v "${DATASET_DIRECTORY}":/root/datasets \
  -e "DISPLAY=unix:0.0" \
  -v="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
  --privileged \
  guilieb/doccreator:latest \
  DocCreator-master/build/software/DocCreator/DocCreator
