#!/usr/bin/env python3
#
# Copyright (c) 2022 − Present Guillaume Bernard <contact@guillaume-bernard.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import argparse
import os
import re
from collections import defaultdict
from pathlib import Path

import numpy
from document_tracking_resources.corpus import NewsCorpus, NewsCorpusWithDenseFeatures
from pandas import DataFrame
from tqdm import tqdm


def main(args: argparse.Namespace):
    # Load the corpus dataframe
    corpus = NewsCorpusWithDenseFeatures.from_pickle_file(args.corpus).as_dataframe()
    corpus = corpus[NewsCorpus.DOCUMENT_COLUMNS + NewsCorpus.GROUND_TRUTH_COLUMNS]
    corpus.index = corpus.index.astype(str)

    # Load content files
    files_in_directory = defaultdict(list)
    for file in os.listdir(args.ocr_files_directory):
        files_in_directory[file.split(".")[0]].append(file)

    output_corpus = DataFrame(columns=corpus.columns)
    for index, _ in tqdm(
        corpus.iterrows(),
        total=len(corpus),
        desc=f"Load segmented documents ({Path(args.corpus).stem})",
    ):
        for chunk_file in files_in_directory[index]:
            chunk_nb = int(re.findall(r".*_chunk_([0-9]).*", chunk_file)[0])
            index_new = f"{index}_{chunk_nb}"
            output_corpus.at[index_new] = corpus.loc[index]

            # Remove title except for the first chunk
            if chunk_nb > 1:
                output_corpus.at[index_new, "title"] = numpy.nan

            # Load chunk text
            with open(
                Path(args.ocr_files_directory) / chunk_file, mode="r", encoding="utf-8"
            ) as ichunk:
                output_corpus.at[index_new, "text"] = ichunk.read()

    Path(args.output_file).parent.mkdir(parents=True, exist_ok=True)
    output_corpus.to_pickle(args.output_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="""
        Load text files to populate the specified field of the input corpus.
    """
    )

    parser.add_argument(
        "--corpus", type=str, required=True, help="Path to the pickle dataset file."
    )
    parser.add_argument(
        "--ocr-files-directory",
        type=str,
        required=True,
        help="Where text files are located, with document id as name (before any extension).",
    )
    parser.add_argument(
        "--output-file",
        "-o",
        type=str,
        required=True,
        help="The name of the new dataset with the updated field.",
    )

    main(parser.parse_args())
