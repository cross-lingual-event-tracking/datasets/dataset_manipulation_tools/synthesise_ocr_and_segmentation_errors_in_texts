#!/usr/bin/env python3
#
# Copyright (c) 2022 − Present Guillaume Bernard <contact@guillaume-bernard.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
import argparse
import re
import time
from pathlib import Path

import fastwer
import pandas
from pandas import DataFrame


def short_dataset_name_from_file(dataset_file: str):
    return Path(dataset_file).stem.split("_")[-1]


def harmonize_text(text: str):
    return re.sub(" +", " ", text.replace("\n", " ").replace("\f", " "))


def compute_cer_and_wer_on_dataframe(input_dataframe: DataFrame):
    dataframe_with_cer_and_wer = input_dataframe.copy()
    for index, row in input_dataframe.iterrows():
        original_text = harmonize_text(row["orig_text"])
        degraded_text = harmonize_text(row["deg_text"])
        cer = float(fastwer.score_sent(degraded_text, original_text, char_level=True))
        wer = float(fastwer.score_sent(degraded_text, original_text, char_level=False))
        dataframe_with_cer_and_wer.at[index, "cer"] = round(cer, 3)
        dataframe_with_cer_and_wer.at[index, "wer"] = round(wer, 3)
    return dataframe_with_cer_and_wer


def compute_cer_and_wer_between_two_datasets_files(
    original_dataset_file: str, damaged_dataset_file: str
):
    results = DataFrame(columns=["cer", "wer"])
    original_dataset = pandas.read_pickle(original_dataset_file)
    original_dataset.index = original_dataset.index.astype(int)
    degraded_dataset = pandas.read_pickle(damaged_dataset_file)
    degraded_dataset.index = degraded_dataset.index.astype(int)
    comparison_df = DataFrame(
        index=original_dataset.index, columns=["orig_text", "deg_text"]
    )
    comparison_df["orig_text"] = original_dataset.text
    comparison_df["deg_text"] = degraded_dataset.text
    degradation_evaluation = compute_cer_and_wer_on_dataframe(comparison_df)
    results.at[
        short_dataset_name_from_file(damaged_dataset_file)
    ] = degradation_evaluation[["cer", "wer"]].mean()
    return results.sort_index().T


def main(args: argparse.Namespace):
    start_time = time.process_time()
    synthesis = compute_cer_and_wer_between_two_datasets_files(
        args.original_dataset, args.damaged_dataset
    )
    synthesis.to_csv(args.output_file)
    print(
        f" {short_dataset_name_from_file(args.damaged_dataset)} ({time.process_time() - start_time:.5f} sec.)".center(
            80, "#"
        )
    )
    print(synthesis)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Evaluate CER/WER by comparing an original dataset with another one the is damaged."
    )
    parser.add_argument(
        "--original-dataset",
        required=True,
        type=str,
        help="Path to the original dataset that contains clean data.",
    )
    parser.add_argument(
        "--damaged-dataset",
        required=True,
        type=str,
        help="Path to the damaged dataset.",
    )
    parser.add_argument(
        "--output-file",
        required=True,
        type=str,
        help="Where to export the statistics file.",
    )
    main(parser.parse_args())
