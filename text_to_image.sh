#!/bin/bash

# Convert the text files of a directory into images, trying to reproduce as better as possible a perfect scan of
# historical documents (because there is no degradation, no rotation, and the text is either not an historical one).
# Param 1: the directory that contain the .txt files with the text to convert.
text_directory="${1}"

# Rotation: https://imagemagick.org/script/command-line-options.php#rotate
# Noise: https://imagemagick.org/script/command-line-options.php#noise
# Blur: https://imagemagick.org/script/command-line-options.php#blur
# Fonts
# - Perpetua
# - Times Roman (used by Times newspaper)
# - ?

declare -r BACKGROUND_COLOR="#FFFFFF"
declare -r TEXT_COLOR="black"
for file in "./${text_directory}/"*".txt"; do
  echo "${file}"
  convert -size 750x \
    -bordercolor "${BACKGROUND_COLOR}" -border 20x20 \
    -fill "${TEXT_COLOR}" -pointsize 24 -font "Times-Roman" \
    -background "${BACKGROUND_COLOR}" \
    caption:@"${file}" \
    "${file%%.txt}".png
done
